NVIDIA EGX Platform
===================

The [NVIDIA EGX Platform](https://www.nvidia.com/en-us/data-center/products/egx/) is an optimized software stack for AI workloads running at the edge which includes the NVIDIA GPU Operator for deploying and managing the GPU components necessary for GPU-enabled Kubernetes.

In DeepOps 22.01 and before, we included Ansible playbooks for installing and validating the EGX Platform in `playbooks/nvidia-egx`.

Up-to-date Ansible playbooks and install guides for this stack can now be found in the [dedicated NVIDIA/egx-platform repository](https://github.com/NVIDIA/egx-platform).
